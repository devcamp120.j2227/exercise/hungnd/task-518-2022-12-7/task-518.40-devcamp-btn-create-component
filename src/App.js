import "bootstrap/dist/css/bootstrap.min.css"

import Parent from "./components/Parent";

import './App.css';

function App() {
  return (
    <div >
      <Parent/>
    </div>
  );
}

export default App;
