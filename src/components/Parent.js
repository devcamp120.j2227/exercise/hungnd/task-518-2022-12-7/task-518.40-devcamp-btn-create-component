import { Component } from "react";
import Display from "./Display";

class Parent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            display: false
        }
    }
    btnUpdateDisplay = () => {
        this.setState({
            display: true
        })
    }
    btnDestroyClick = () => {
        this.setState({
            display: false
        })
    }
    render() {
        return (
            <div className="text-center mt-5 container bg-secondary">
                <div>
                    {this.state.display ? <button className="btn btn-danger" onClick={this.btnDestroyClick }>Destroy Component</button> : <button className="btn btn-info text-white" onClick={this.btnUpdateDisplay}>Create Component</button>}
                </div>
                <div>
                    {this.state.display ? <Display /> : null}
                </div>

            </div>
        )
    }
}
export default Parent;